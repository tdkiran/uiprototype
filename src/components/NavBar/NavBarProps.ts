export interface INavBarProps {
  data: INavBarObject[];
}
export interface INavBarObject {
  additional?: IAnchor[];
  content?: IContent[];
  items: IAnchor[] | null;
  title: IAnchor;
}
export interface IContent {
  key: string;
  templateName: string;
}
export interface IAnchor {
  title: string;
  url?: string;
}
