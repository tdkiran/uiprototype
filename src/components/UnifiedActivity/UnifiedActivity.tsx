import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faGlobe,
  faSearch,
  faUserAstronaut
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as React from "react";
import unifiedActivityData from "../../data/unifiedActivityData";
import ContactMenu from "../ContactMenu/ContactMenu";
import GlobalSiteMenu from "../GlobalSiteMenu/GlobalSiteMenu";
import GlobalSiteMenuData from "./../../data/globalSitesMenuData";
import UserLoginMenuData from "./../../data/UserLoginMenuData";
import UserLoginMenu from "./../UserLoginMenu/UserLoginMenu";
import "./UnifiedActivity.css";

library.add(faGlobe, faSearch, faUserAstronaut);
class UnifiedActivity extends React.Component {
  private constructor(Props:{}){
    super(Props);
    this.hideOtherSubMenus = this.hideOtherSubMenus.bind(this);

  }
  public render(){
  return (
    <div id="UnifiedActivity" role="application">
      <span className="ua-icon" tabIndex={0} onFocus={this.hideOtherSubMenus}>
        <FontAwesomeIcon icon="search" />
      </span>

      <ContactMenu data={unifiedActivityData.contactMenu} />

      <GlobalSiteMenu data={GlobalSiteMenuData} />

      <UserLoginMenu data={UserLoginMenuData} />
    </div>
  );
}
 private hideOtherSubMenus(e: React.FocusEvent<HTMLElement>) {
    e.preventDefault();
    const popups = Array.from(document.querySelectorAll(".popup")!);
    for (const element of popups) {
      element.removeAttribute("id");
    }
  }
};
export default UnifiedActivity;
