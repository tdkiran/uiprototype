export interface IContactMenuProps {
  data: IContactMenuData;
}
interface IContactMenuData {
  heading: string;
  phone?: string[];
  email?: string[];
  links?: ILinks[];
}
interface ILinks {
  label: string;
  url: string;
}
