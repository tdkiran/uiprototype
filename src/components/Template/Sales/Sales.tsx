import * as React from "react";
import "./Sales.css";
const template1 = () => {
  return (
    <div className="template_main" tabIndex={-1}>
      <div className="template_header" tabIndex={-1}>
        <h1 tabIndex={0}>Sales Cloud</h1>
        <p tabIndex={0}>
          Sell faster and smarter with the worls's #1 sales platform
        </p>
        <button tabIndex={0}>Learn More</button>
      </div>
      <div className="template_content">
        <ul className="template_list">
          <li>
            <a href="">Sales CPQ</a>
          </li>
          <li>
            <a href="">Sales Force Inbox></a>
          </li>
          <li>
            <a href="">Salesforce Essentials</a>
          </li>
          <li>
            <a href="">App Exchange</a>
          </li>
          <li>
            <a href="">Pardot</a>
          </li>
          <li>
            <a href="">Partner Communities</a>
          </li>
          <li>
            <a href="">Sales Could Einsten</a>
          </li>
          <li>
            <a href="">SF Mobile</a>
          </li>
        </ul>
      </div>
    </div>
  );
};
export default template1;
