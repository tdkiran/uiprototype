import * as React from "react";
import "./Commerce.css";
const template1 = () => {
  return (
    <div className="template_main" tabIndex={-1}>
      <div className="template_header" tabIndex={-1}>
        <h1 tabIndex={0}>Commerce</h1>
        <p tabIndex={0}>
          Unify the shopping experince with number one cloud app.
        </p>
        <button tabIndex={0}>Learn More</button>
      </div>
      <div className="template_content">
        <ul className="template_list">
          <li>
            <a href="">Sales CPQ</a>
          </li>
          <li>
            <a href="">Sales Force Inbox></a>
          </li>
          <li>
            <a href="">Salesforce Essentials</a>
          </li>
          <li>
            <a href="">App Exchange</a>
          </li>
          <li>
            <a href="">Pardot</a>
          </li>
          <li>
            <a href="">Partner Communities</a>
          </li>
          <li>
            <a href="">Sales Could Einsten</a>
          </li>
          <li>
            <a href="">SF Mobile</a>
          </li>
        </ul>
      </div>
    </div>
  );
};
export default template1;
