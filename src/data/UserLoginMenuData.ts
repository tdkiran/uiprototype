const data = {
  container1: {
    imgUrl:
      "https://c1.sfdcstatic.com/content/dam/web/en_us/www/images/nav/logo-dropdown-salesforce-login.png",
    title: "Login"
  },
  container2: {
    subTitle: "Marketting Cloud, Trail ahead,",
    title: "Other logins",
    urls: [
      {
        label: "Marketting Cloud",
        url: "www.salesforce.com"
      },
      {
        label: "Trail ahead",
        url: "www.salesforce.com"
      },
      {
        label: "Advertising Studio",
        url: "www.salesforce.com"
      },
      {
        label: "Advertising Studios",
        url: "www.salesforce.com"
      }
    ]
  },
  title: "User Login"
};
export default data;
