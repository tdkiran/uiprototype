const data = [
  {
    additional: [
      {
        title: "Product Overview",
        url: "https://www.salesforce.com/"
      },
      {
        title: "Pricing",
        url: "https://www.salesforce.com/"
      },
      {
        title: "Small Business",
        url: "https://www.salesforce.com/"
      },
      {
        title: "Intelligence",
        url: "https://www.salesforce.com/"
      }
    ],
    content: [
      { key: "Sales", templateName: "Sales" },
      { key: "Commerce", templateName: "Commerce" }
    ],
    items: [
      {
        title: "Sales",
        url: "https://www.salesforce.com/products/sales-cloud/overview/"
      },
      {
        title: "Service",
        url: "https://www.salesforce.com/products/service-cloud/overview/"
      },
      {
        title: "Marketting",
        url: "https://www.salesforce.com/products/marketing-cloud/overview/"
      },
      {
        title: "Commerce",
        url: "https://www.salesforce.com/products/commerce-cloud/overview/"
      },
      {
        title: "Collabration",
        url: "https://www.salesforce.com/products/quip/overview/"
      },
      {
        title: "Engagement",
        url: "https://www.salesforce.com/products/integration-cloud/overview/"
      },
      {
        title: "Platform",
        url: "https://www.salesforce.com/products/platform/overview/"
      }
    ],
    title: {
      title: "Products",
      url: "https://www.salesforce.com/"
    }
  },
  {
    items: [
      { title: "By Industry", url: "" },
      { title: "For Small Business", url: "" },
      { title: "By Role", url: "" },
      { title: "For Startups", url: "" },
      { title: "For Nonprofits", url: "" }
    ],
    title: {
      title: "Solutions",
      url: "https://www.salesforce.com/"
    }
  },
  {
    items: [
      { title: "Success Cloud", url: "" },
      { title: "Learning and Turorials", url: "" },
      { title: "Communities", url: "" },
      { title: "Resources", url: "" }
    ],

    title: {
      title: "Support & Services",
      url: "https://www.salesforce.com/"
    }
  },
  {
    items: [
      { title: "Customer Stories", url: "" },
      { title: "Small Business", url: "" },
      { title: "Enterprise", url: "" }
    ],
    title: {
      title: "Customer Success",
      url: "https://www.salesforce.com/"
    }
  },
  {
    items: null,
    title: {
      title: "About",
      url: "https://www.salesforce.com/company/about-us/"
    }
  }
];
export default data;
